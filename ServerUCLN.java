import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerUCLN {
    public static void main(String[] args) {
       new ServerUCLN().run();
    }
    private void run(){
        int port=6394;
        try {
            ImplUCLN tinhToan = new ImplUCLN();
            Registry registry = LocateRegistry.createRegistry(port);
            Naming.rebind("rmi://localhost:"+port+"/ImplUCLN", tinhToan);
            System.out.println("Server is running ...");
        } catch (RemoteException ex) {
            Logger.getLogger(ServerUCLN.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ServerUCLN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
