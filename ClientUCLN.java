import java.net.MalformedURLException;
import java.rmi.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientUCLN {
    public static void main(String[] args) {
        // TODO code application logic here
        new ClientUCLN().run();
    }
    
    private void run(){
        int port=6394;
         
        try {
            UCLN ucln = (UCLN) Naming.lookup("rmi://localhost:"+port+"/ImplUCLN");
            
            Scanner nhap = new Scanner(System.in);
            System.out.print("Nhap a: ");
            int a = nhap.nextInt();
            System.out.print("Nhap b: ");
            int b = nhap.nextInt();          
            System.out.println("UCLN la :"+ucln.UCLN(a, b));
            System.out.println("BCNN la:"+((a*b)/(ucln.UCLN(a, b))));

        } catch (NotBoundException ex) {
            Logger.getLogger(ClientUCLN.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ClientUCLN.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(ClientUCLN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
