import java.rmi.*;
import java.rmi.server.*;

public class ImplUCLN extends UnicastRemoteObject implements UCLN{
    public ImplUCLN() throws RemoteException {
    }

    @Override
    public int UCLN(int a,int b) throws RemoteException {
      while(a != b)
        {
            if(a > b)
                a = a - b;
            else
                b = b - a;
        }
        return a;
    }
}
